include <iostream>

int main(){

  int number;
  std::cout << "Enter an integer: ";
  std::cin >> number;

  bool isPrime = true;
  for (int i = 1; i < number; ++i)
    if (!number % i)
      isPrime = false;

  if (isPrime)
    std::cout << "Prime\n";
  else
    std::cout << "Not prime\n";
  
  return 0;
}
